#
#  Requires GNU make.
#

PROGRAM=carob

# CC=gcc
# CC=clang-6.0
# CC=tcc -DSDL_DISABLE_IMMINTRIN_H

MISC=-std=c99
WARNINGS=-Wall -Wextra -Wno-implicit-fallthrough -Wno-sign-compare -Wno-shadow -Wno-unused-parameter
OPTIMISE=-O1

# default flags for compiler, preprocessor and linker
CFLAGS ?= $(MISC) $(OPTIMISE) $(WARNINGS)
CPPFLAGS ?=
LDFLAGS ?= $(OPTIMISE)
LIBS ?=

OBJ_DIR=_build

DUMMY=$(OBJ_DIR)/zzdummy

#----- Libraries ----------------------------------------------

CPPFLAGS += -I/usr/include/SDL2

LIBS += -lSDL2 -lSDL2_mixer


#----- Object files ----------------------------------------------

OBJS = \
	$(OBJ_DIR)/am_map.o   \
	$(OBJ_DIR)/d_event.o   \
	$(OBJ_DIR)/d_items.o   \
	$(OBJ_DIR)/d_iwad.o   \
	$(OBJ_DIR)/d_loop.o   \
	$(OBJ_DIR)/d_main.o   \
	$(OBJ_DIR)/d_mode.o   \
	$(OBJ_DIR)/d_net.o   \
	$(OBJ_DIR)/doomdef.o   \
	$(OBJ_DIR)/doomstat.o   \
	$(OBJ_DIR)/dstrings.o   \
	$(OBJ_DIR)/f_finale.o   \
	$(OBJ_DIR)/f_wipe.o   \
	$(OBJ_DIR)/g_game.o   \
	$(OBJ_DIR)/hu_lib.o   \
	$(OBJ_DIR)/hu_stuff.o   \
	$(OBJ_DIR)/i_input.o     \
	$(OBJ_DIR)/i_main.o      \
	$(OBJ_DIR)/info.o        \
	$(OBJ_DIR)/i_sdlsound.o  \
	$(OBJ_DIR)/i_oplmusic.o  \
	$(OBJ_DIR)/i_png.o     \
	$(OBJ_DIR)/i_sound.o   \
	$(OBJ_DIR)/i_system.o  \
	$(OBJ_DIR)/i_timer.o   \
	$(OBJ_DIR)/i_video.o   \
	$(OBJ_DIR)/m_argv.o   \
	$(OBJ_DIR)/m_bbox.o   \
	$(OBJ_DIR)/m_cheat.o  \
	$(OBJ_DIR)/m_config.o \
	$(OBJ_DIR)/m_controls.o   \
	$(OBJ_DIR)/m_fixed.o  \
	$(OBJ_DIR)/m_menu.o   \
	$(OBJ_DIR)/m_misc.o   \
	$(OBJ_DIR)/m_random.o \
	$(OBJ_DIR)/memio.o \
	$(OBJ_DIR)/midifile.o \
	$(OBJ_DIR)/mus2mid.o \
	$(OBJ_DIR)/opl3.o \
	$(OBJ_DIR)/opl_sdl.o \
	$(OBJ_DIR)/opl_queue.o \
	$(OBJ_DIR)/p_ceilng.o  \
	$(OBJ_DIR)/p_doors.o   \
	$(OBJ_DIR)/p_enemy.o   \
	$(OBJ_DIR)/p_floor.o   \
	$(OBJ_DIR)/p_inter.o   \
	$(OBJ_DIR)/p_lights.o  \
	$(OBJ_DIR)/p_map.o     \
	$(OBJ_DIR)/p_maputl.o  \
	$(OBJ_DIR)/p_mobj.o   \
	$(OBJ_DIR)/p_plats.o  \
	$(OBJ_DIR)/p_pspr.o   \
	$(OBJ_DIR)/p_saveg.o  \
	$(OBJ_DIR)/p_setup.o   \
	$(OBJ_DIR)/p_sight.o  \
	$(OBJ_DIR)/p_spec.o   \
	$(OBJ_DIR)/p_switch.o \
	$(OBJ_DIR)/p_telept.o \
	$(OBJ_DIR)/p_tick.o   \
	$(OBJ_DIR)/p_user.o   \
	$(OBJ_DIR)/r_bsp.o    \
	$(OBJ_DIR)/r_data.o   \
	$(OBJ_DIR)/r_draw.o   \
	$(OBJ_DIR)/r_main.o   \
	$(OBJ_DIR)/r_plane.o  \
	$(OBJ_DIR)/r_segs.o   \
	$(OBJ_DIR)/r_sky.o    \
	$(OBJ_DIR)/r_things.o   \
	$(OBJ_DIR)/sounds.o   \
	$(OBJ_DIR)/s_sound.o  \
	$(OBJ_DIR)/st_lib.o   \
	$(OBJ_DIR)/st_stuff.o \
	$(OBJ_DIR)/tables.o   \
	$(OBJ_DIR)/v_video.o     \
	$(OBJ_DIR)/wi_stuff.o \
	$(OBJ_DIR)/w_file.o   \
	$(OBJ_DIR)/w_main.o   \
	$(OBJ_DIR)/w_merge.o  \
	$(OBJ_DIR)/w_wad.o    \
	$(OBJ_DIR)/z_zone.o

$(OBJ_DIR)/%.o: ./%.c
	$(CC) $(CFLAGS) $(CPPFLAGS) -o $@ -c $<


#----- Targets -----------------------------------------------

all: $(DUMMY) $(PROGRAM)

clean:
	rm -f $(PROGRAM) $(OBJ_DIR)/*.[oa]
	rm -f core core.* ERRS

$(PROGRAM): $(OBJS)
	$(CC) $^ -o $@ $(LDFLAGS) $(LIBS)

# this is used to create the OBJ_DIR directory
$(DUMMY):
	mkdir -p $(OBJ_DIR)
	@touch $@

.PHONY: all clean

#--- editor settings ------------
# vi:ts=8:sw=8:noexpandtab
